package com.belajarspringbootweb.webhelloworld.controller;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class HomeController {

    @GetMapping("/hello")
    public String HelloWorld(){
        return "hello-world";
    }

}
